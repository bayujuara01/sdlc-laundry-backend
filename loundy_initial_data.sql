
USE `loundydb`;

INSERT IGNORE INTO role(id, authority, created_at) VALUES
(1, 'USER', CURRENT_TIMESTAMP()),
(2, 'PARTNER', CURRENT_TIMESTAMP()),
(3, 'ADMIN', CURRENT_TIMESTAMP());

INSERT IGNORE INTO address(id, created_at, latitude, longitude) VALUES 
(1, CURRENT_TIMESTAMP(), -6.2555415,106.6152343),
(2, CURRENT_TIMESTAMP(), -6.2551827,106.6142198),
(3, CURRENT_TIMESTAMP(), -6.2555415,106.6152343),
(4, CURRENT_TIMESTAMP(), -6.2562054,106.6074112);

# Password : Bayujuara01
INSERT IGNORE INTO user(id, email, username, `password`, address_id, created_at) VALUES 
(1, 'ariefyantobayu.admin@gmail.com', 'Bayu Admin Ariefyanto', '$2a$12$kz3fhzN1VWsg72.uhRFoQuqJoMVqmP1uS1QWPANHSUVJaxsiuMYIm', 1, CURRENT_TIMESTAMP()),
(2, 'ariefyantobayu.user@gmail.com', 'Bayu User Ariefyanto', '$2a$12$kz3fhzN1VWsg72.uhRFoQuqJoMVqmP1uS1QWPANHSUVJaxsiuMYIm', 2, CURRENT_TIMESTAMP()),
(3, 'ariefyantobayu.partner@gmail.com', 'Bayu Partner Ariefyanto', '$2a$12$kz3fhzN1VWsg72.uhRFoQuqJoMVqmP1uS1QWPANHSUVJaxsiuMYIm', 3, CURRENT_TIMESTAMP()),
(4, 'ariefyantobayu@gmail.com', 'Bayu Seno Ariefyanto', '$2a$12$kz3fhzN1VWsg72.uhRFoQuqJoMVqmP1uS1QWPANHSUVJaxsiuMYIm', 4, CURRENT_TIMESTAMP());

INSERT IGNORE INTO user_roles VALUES 
(1,3),
(2,1),
(3,2),
(4,1);

INSERT IGNORE INTO status(id, status_name) VALUES
(1, 'UNPAID'),
(2, 'PAID'),
(3, 'PROCESS'),
(4, 'SHIPMENT'),
(5, 'SUCCESS');

INSERT IGNORE INTO item (id, `name`, price, created_at) VALUES 
(1, 'So Klin 1L', 37000, CURRENT_TIMESTAMP()),
(2, 'Downey 1L', 31000, CURRENT_TIMESTAMP()),
(3, 'Pemutih 1L', 24000, CURRENT_TIMESTAMP());

INSERT IGNORE INTO service (id, `name`, price, created_at) VALUES
(1, 'Paket Kiloan 5 (5Kg)', 35000, CURRENT_TIMESTAMP()),
(2, 'Paket Kiloan 3 (3Kg)', 22000, CURRENT_TIMESTAMP()),
(3, 'Paket Kiloan 1 (1Kg)', 15000, CURRENT_TIMESTAMP());

INSERT IGNORE INTO transaction (id, grand_total, created_at, status_id, partner_id, user_id) VALUES
(1, 30000, CURRENT_TIMESTAMP(), 1, 3, 1),
(2, 35000, CURRENT_TIMESTAMP(), 1, 3, 4);

INSERT IGNORE INTO transaction_detail (id, price, qty, total, created_at, service_id, transaction_id) VALUES
(1, 15000, 2, 30000, CURRENT_TIMESTAMP(), 3, 1),
(2, 35000, 1, 35000, CURRENT_TIMESTAMP(), 1, 2);

INSERT IGNORE INTO orders (id, grand_total, payment_status, created_at, status_id, admin_id, partner_id) VALUES 
(1, 62000, 0, CURRENT_TIMESTAMP(), 1, 1, 2),
(2, 55000, 0, CURRENT_TIMESTAMP(), 1, 1, 2);

INSERT IGNORE INTO order_detail (id, qty, total, created_at, item_id, order_id) VALUES 
(1, 2, 62000, CURRENT_TIMESTAMP(), 2, 1),
(2, 1, 62000, CURRENT_TIMESTAMP(), 2, 2),
(3, 1, 62000, CURRENT_TIMESTAMP(), 3, 2);




