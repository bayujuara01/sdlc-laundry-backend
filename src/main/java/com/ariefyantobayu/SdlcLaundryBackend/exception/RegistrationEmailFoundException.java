package com.ariefyantobayu.SdlcLaundryBackend.exception;

public class RegistrationEmailFoundException extends RuntimeException {
    public RegistrationEmailFoundException() {
        super("Error : Registration Email Found Exception");
    }
}
