package com.ariefyantobayu.SdlcLaundryBackend.exception;

public class RecordNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 7209768090421269700L;

	public RecordNotFoundException(String message) {
		super(message);
	}
}
