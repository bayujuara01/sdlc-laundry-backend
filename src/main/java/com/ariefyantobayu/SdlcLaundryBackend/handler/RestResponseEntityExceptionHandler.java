package com.ariefyantobayu.SdlcLaundryBackend.handler;

import java.util.HashMap;
import java.util.Map;

import com.ariefyantobayu.SdlcLaundryBackend.exception.RecordNotFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.exception.RegistrationEmailFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.response.ErrorResponse;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {

        System.out.print(ex.getMessage());
		Map<String, String> errors = new HashMap<>();

		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		}
        
        return ResponseEntity.badRequest().body(new ErrorResponse(HttpStatus.BAD_REQUEST, "ARGUMENT NOT VALID", errors));
    }
    
    @ExceptionHandler(value = {RecordNotFoundException.class})
    protected ResponseEntity<Response> handlerRecordNotFound(RuntimeException ex, WebRequest request) {
    	return Responses.ofError(HttpStatus.NOT_FOUND, "Not Found", "Content not found as ecpected");
    }

    @ExceptionHandler( value = {BadCredentialsException.class})
    protected  ResponseEntity<Response> handleBadCredentials(RuntimeException ex) {
        return Responses.ofError(HttpStatus.UNAUTHORIZED, "Unauthorized", "Don't have access to this path");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler( value = {RegistrationEmailFoundException.class})
    protected ResponseEntity<Response> handleErrorRegistration(RuntimeException ex) {
        return Responses.ofBadRequest("Bad Request", "Email already used");
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.badRequest().body(new ErrorResponse(HttpStatus.BAD_REQUEST, "Request body haven't valid formation", "BAD REQUEST"));
    }
}