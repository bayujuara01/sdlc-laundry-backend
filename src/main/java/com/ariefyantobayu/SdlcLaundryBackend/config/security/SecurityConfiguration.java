package com.ariefyantobayu.SdlcLaundryBackend.config.security;

import javax.servlet.http.HttpServletResponse;

import com.ariefyantobayu.SdlcLaundryBackend.config.auth.ApiJWTAuthenticationFilter;
import com.ariefyantobayu.SdlcLaundryBackend.config.auth.ApiJWTAuthorizationFilter;
import com.ariefyantobayu.SdlcLaundryBackend.security.UserDetailServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration {

    public static class Roles {
        public static final String ANONYMOUS = "ANONYMOUS";
		public static final String USER = "USER";
        public static final String PARTNER = "PARTNER";
		static public final String ADMIN = "ADMIN";
    }
    
    @Configuration
    @Order(2)
    protected static class SecurityConfigurer extends WebSecurityConfigurerAdapter {

        @Autowired
        private BCryptPasswordEncoder bCryptPasswordEncoder;

        @Autowired
        private UserDetailServiceImpl userDetailServiceImpl;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailServiceImpl)
                .passwordEncoder(bCryptPasswordEncoder);
            
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            
            http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/test/*/lol").permitAll()
                .antMatchers("/authenticate").permitAll()
                .antMatchers("/hello").hasAuthority(Roles.USER)
                .antMatchers("/admin").hasAnyAuthority(Roles.ADMIN)
                .antMatchers("/customer/login").permitAll()
                .antMatchers("/customer/register").permitAll()
                .antMatchers("/customer/**").hasAnyAuthority(Roles.USER)
                .antMatchers("/partner/**").permitAll()
                .antMatchers("/admin/**").hasAuthority(Roles.ADMIN)
                .antMatchers("/**").denyAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and()
                .addFilterBefore(new ApiJWTAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new ApiJWTAuthorizationFilter((authenticationManager())), BasicAuthenticationFilter.class)
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers(
                "/api/auth/**"
            );
        }

        @Override
	    @Bean
	    public AuthenticationManager authenticationManagerBean() throws Exception {
		    return super.authenticationManagerBean();
	    }
        
    }

}
