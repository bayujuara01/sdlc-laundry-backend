package com.ariefyantobayu.SdlcLaundryBackend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * A class used for configuration a cors
 */
@Configuration
public class CorsConfiguration 
{
    /**
     * A Depedency Injection that automatically run on first start of spring app
     * that configure a cors to allow some specific domain
     * @return WebMvcConfigurer that automatically registry a Cors.
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() 
    {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:3000","http://localhost:8085", "http://localhost:8090");
            }
        };
    }
}