package com.ariefyantobayu.SdlcLaundryBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdlcLaundryBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdlcLaundryBackendApplication.class, args);
	}

}
