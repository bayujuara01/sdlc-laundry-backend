package com.ariefyantobayu.SdlcLaundryBackend.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference; 

@Table(name = "status")
@Entity(name = "status")
public class Status {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "STATUS_NAME")
    private String statusName;
    
    @JsonBackReference(value = "statusRefs")
//    @JsonIgnore
    @OneToMany(mappedBy = "status")
    private List<Orders> orders;

    public Status() {}

    public Status(Long id, String statusName) {
        this.id = id;
        this.statusName = statusName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    
    public List<Orders> getOrders() {
		return orders;
	}
    
    public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}
    

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}