package com.ariefyantobayu.SdlcLaundryBackend.entity;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "order_detail")
@Table(name = "ORDER_DETAIL")
public class OrderDetail {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "QTY")
	private Integer qty;

	@Column(name = "TOTAL")
	private Integer total;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@Column(name = "UPDATED_AT")
	private Timestamp updated_at;

	@Column(name = "DELETED_AT")
	private Timestamp deleted_at;

	@JsonBackReference(value = "itemOrderDetailRefs")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id",
			referencedColumnName = "id",
			nullable = false, foreignKey = @ForeignKey(name = "FK_ordersDetail_item"))
	private Item item;

	@JsonBackReference(value = "orderDetailsRefs")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id",
			referencedColumnName = "id",
			nullable = false, foreignKey = @ForeignKey(name = "FK_ordersDetail_order"))
	private Orders order;

	public OrderDetail() {}

	public OrderDetail(int qty, int total, Item item, Orders order) {
		super();
		this.qty = qty;
		this.total = total;
		this.item = item;
		this.order = order;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Timestamp getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Timestamp deleted_at) {
		this.deleted_at = deleted_at;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Orders getOrder() {
		return order;
	}

	public void setOrder(Orders order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "OrderDetail [id=" + id + ", qty=" + qty + ", total=" + total + ", created_at=" + created_at
				+ ", updated_at=" + updated_at + ", deleted_at=" + deleted_at + ", item=" + item + ", order=" + order
				+ "]";
	}
	
	
}
