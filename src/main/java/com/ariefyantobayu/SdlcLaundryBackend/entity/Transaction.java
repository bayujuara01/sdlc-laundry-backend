package com.ariefyantobayu.SdlcLaundryBackend.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "transaction")
@Table(name = "TRANSACTION")
public class Transaction {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "GRAND_TOTAL")
	private Integer grandTotal;

	@Column(name = "PAYMENT_STATUS", nullable = false)
	private Boolean paymentStatus;

	@Column(name = "LATITUDE", precision = 10, scale = 8)
	private Double latitude;

	@Column(name = "LONGITUDE", precision = 11, scale = 8)
	private Double longitude;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@Column(name = "UPDATED_AT")
	private Timestamp updated_at;

	@Column(name = "DELETED_AT")
	private Timestamp deleted_at;

	@JsonBackReference(value = "transactionCustomerRefs")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_transaction_user_customer"))
	private User user;

	@JsonBackReference(value = "transactionPartnerRefs")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partner_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_transaction_user_partner"))
	private User partner;

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_transaction_status"))
	private Status status;

	@JsonManagedReference(value = "transactionDetailsRefs")
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "transaction")
	private List<TransactionDetail> transactionDetails;

	public Transaction() {}

	public Transaction(Integer grandTotal, boolean paymentStatus, double latitude, double longitude, User user) {
		super();
		this.grandTotal = grandTotal;
		this.paymentStatus = paymentStatus;
		this.latitude = latitude;
		this.longitude = longitude;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Boolean getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Timestamp getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Timestamp deleted_at) {
		this.deleted_at = deleted_at;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getPartner() {
		return partner;
	}

	public void setPartner(User partner) {
		this.partner = partner;
	}

	public List<TransactionDetail> getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public Status getStatus() {
		return status;
	}
	public String getStatusName() {
		return status.getStatusName();
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getPartnerId() {
		return partner.getId();
	}

	public Long getUserId() {
		return user.getId();
	}
	
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", grandTotal=" + grandTotal + ", paymentStatus=" + paymentStatus
				+ ", latitude=" + latitude + ", longitude=" + longitude + ", created_at=" + created_at + ", updated_at="
				+ updated_at + ", deleted_at=" + deleted_at + ", user=" + user + "]";
	}

}
