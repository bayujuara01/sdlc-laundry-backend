package com.ariefyantobayu.SdlcLaundryBackend.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "orders")
@Table(name = "orders")
public class Orders {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "GRAND_TOTAL")
	private Integer grandTotal;

	@Column(name = "ORDERCOL")
	private String OrderCol;

	@Column(name = "PAYMENT_STATUS", nullable = false)
	private Boolean paymentStatus;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@Column(name = "UPDATED_AT")
	private Timestamp updated_at;

	@Column(name = "DELETED_AT")
	private Timestamp deleted_at;

	@JsonManagedReference(value = "orderDetailsRefs")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.PERSIST)
	private List<OrderDetail> orderDetails;

//	@JsonBackReference(value = "statusRefs")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_orders_status"))
	private Status status;

	@JsonBackReference(value = "transactionPartnerRefs")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partner_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_orders_user_partner"))
	private User partner;

	@JsonBackReference(value = "transactionAdminRefs")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "admin_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_orders_user_admin"))
	private User admin;

	public Orders() {
	}

	public Orders(int grandTotal, String orderCol, boolean paymentStatus, User partner) {
		super();
		this.grandTotal = grandTotal;
		OrderCol = orderCol;
		this.paymentStatus = paymentStatus;
		this.partner = partner;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Integer grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getOrderCol() {
		return OrderCol;
	}

	public void setOrderCol(String orderCol) {
		OrderCol = orderCol;
	}

	public Boolean isPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Timestamp getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Timestamp deleted_at) {
		this.deleted_at = deleted_at;
	}
	
	public User getPartner() {
		return partner;
	}

	public void setPartner(User partner) {
		this.partner = partner;
	}
	
	public User getAdmin() {
		return admin;
	}
	
	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}
	
	
	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", grandTotal=" + grandTotal + ", OrderCol=" + OrderCol + ", paymentStatus="
				+ paymentStatus + ", created_at=" + created_at + ", updated_at=" + updated_at + ", deleted_at="
				+ deleted_at + ", user=" + partner + "]";
	}

}
