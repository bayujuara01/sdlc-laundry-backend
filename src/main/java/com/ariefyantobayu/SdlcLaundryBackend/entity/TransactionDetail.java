package com.ariefyantobayu.SdlcLaundryBackend.entity;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name = "transaction_detail")
@Table(name = "TRANSACTION_DETAIL")
public class TransactionDetail {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "QTY")
	private Integer qty;

	@Column(name = "TOTAL")
	private Integer total;

	@Column(name = "PRICE")
	private Integer price;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@Column(name = "UPDATED_AT")
	private Timestamp updated_at;

	@Column(name = "DELETED_AT")
	private Timestamp deleted_at;

	@JsonBackReference(value = "transactionDetailsRefs")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transaction_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_transactionDetail_transaction"))
	private Transaction transaction;

	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "service_id",
			referencedColumnName = "id",
			nullable = false,
			foreignKey = @ForeignKey(name = "FK_transactionDetail_service"))
	private Service service;

	public TransactionDetail() {}

	public TransactionDetail(int qty, int total, Transaction transaction, Service service) {
		super();
		this.qty = qty;
		this.total = total;
		this.transaction = transaction;
		this.service = service;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Timestamp getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Timestamp deleted_at) {
		this.deleted_at = deleted_at;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "TransactionDetail [id=" + id + ", qty=" + qty + ", total=" + total + ", created_at=" + created_at
				+ ", updated_at=" + updated_at + ", deleted_at=" + deleted_at + ", transaction=" + transaction
				+ ", service=" + service + "]";
	}

}
