package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ariefyantobayu.SdlcLaundryBackend.entity.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{
	
	@Query(value="SELECT * FROM item WHERE id=?", nativeQuery=true)
	Optional<Item> getItemById(Long id);

}
