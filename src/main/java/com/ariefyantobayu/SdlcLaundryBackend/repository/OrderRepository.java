package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ariefyantobayu.SdlcLaundryBackend.entity.Orders;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Long>{
	@Query( value = "SELECT * FROM orders WHERE orders.status_id = ?1", nativeQuery = true)
	List<Orders> findByStatusId(Long id);
}
