package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.List;

import javax.transaction.Transactional;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomTransaction;
import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomFields;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	@Query(value = ""
			+ "SELECT us_cust.username AS customername, tr.grand_total AS grandTotal, tr.payment_status AS paymentStatus, us_part.username AS partnerName "
			+ "FROM transaction tr " 
			+ "JOIN user us_cust ON tr.user_id = us_cust.id " 
			+ "JOIN user us_part "
			+ "JOIN role r " 
			+ "JOIN user_roles usrole ON usrole.user_id = us_part.id AND r.id = usrole.roles_id "
			+ "WHERE tr.user_id = ?1 AND r.id = 2;", nativeQuery = true)
	List<CustomTransaction.GetTransByUserId> GetTransByUserId(Long user_id);

	@Transactional
	@Modifying
	@Query(value = "INSERT INTO transaction (grand_total, payment_status, latitude, longitude, user_id, created_at) "
			+ "VALUES (?1, ?2, ?3, ?4, ?5, NOW())", nativeQuery = true)
	int createTransaction(int grandTotal, boolean paymentStatus, double latitude, double longitude, Long user_id);
	
	@Query(value="SELECT * FROM transaction WHERE partner_id=?", nativeQuery=true)
	List<Transaction> getAllTransactions(Long partnerId);
	
	@Query(value="SELECT * FROM transaction WHERE status_id=1 AND partner_id=?", nativeQuery=true)
	List<Transaction> getNewTransactions(Long partnerId);
	
	@Query(value="SELECT * FROM transaction WHERE status_id=3 AND partner_id=?", nativeQuery=true)
	List<Transaction> getOnProcessTransactions(Long partnerId);
	
	@Query(value="SELECT * FROM transaction WHERE status_id=5 AND partner_id=?", nativeQuery=true)
	List<Transaction> getCompletedTransactions(Long partner_id);
	
	@Query(value="SELECT SUM(grand_total) from transaction where DATE(created_at) =?1 AND status_id=5 AND partner_id=?2",
			nativeQuery=true)
	Integer getSumGrandTotalByDate(String date, Long partner_id);
	
	@Query(value="SELECT * FROM transaction WHERE partner_id=?1 AND id=?2", nativeQuery=true)
	Optional<Transaction> getTransactionByIdAndPartner(Long idPartner, Long idTransaction);
	
}
