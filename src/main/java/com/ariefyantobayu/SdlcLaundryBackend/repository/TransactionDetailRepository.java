package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.SdlcLaundryBackend.entity.Transaction;
import com.ariefyantobayu.SdlcLaundryBackend.entity.TransactionDetail;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Long> {

	@Query(value="SELECT * FROM transaction_detail where transaction_id=?", nativeQuery=true)
	List<TransactionDetail> getTransactionDetailById(Long id);

}
