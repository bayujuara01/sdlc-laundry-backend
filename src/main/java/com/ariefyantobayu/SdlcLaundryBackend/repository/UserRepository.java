package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.Optional;

import com.ariefyantobayu.SdlcLaundryBackend.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserRepository extends JpaRepository<User, Long> {
    @Query( value = "SELECT * FROM user WHERE user.email = :email",
            nativeQuery = true)
    Optional<User> findByEmail(String email);

    @Query( value = "SELECT user.id, user.email, user.password, user.phone," +
            "user.created_at as Created_at, user.updated_at as Updated_at," +
            "user.deleted_at as Deleted_at, user.username," +
            "user.address_id FROM user WHERE user.email = :email",
            nativeQuery = true)
    Optional<User> findByEmailNative(@Param("email") String email);
}
