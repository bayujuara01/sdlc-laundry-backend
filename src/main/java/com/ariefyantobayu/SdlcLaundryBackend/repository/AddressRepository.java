package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomUser;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
	@Query(value = "SELECT us.username AS username, adr.latitude AS latitude, adr.longitude AS longitude, "
			+ "		( 6371 * acos( cos( radians(?1) ) * "
			+ "				 cos( radians( adr.latitude ) ) * "
			+ "				 cos( radians( adr.longitude ) - radians(?2) ) + "
			+ "				 sin( radians(?1) ) * "
			+ "				 sin( radians( adr.latitude ) ) ) ) AS distance "
			+ "FROM address adr "
			+ "JOIN user us ON us.address_id = adr.id "
			+ "JOIN role r "
			+ "JOIN user_roles usrole ON usrole.user_id = us.id AND r.id = usrole.roles_id "
			+ "WHERE r.id = 2 "
			+ "HAVING distance < ?3 "
			+ "ORDER BY distance "
			+ "LIMIT 0, ?4",
			nativeQuery = true)
	List<CustomUser.GetNearby> GetNearbyPartner(double userLatitude, double userLongitude, int maxDistance, int maxShown);

}
