package com.ariefyantobayu.SdlcLaundryBackend.repository;

import com.ariefyantobayu.SdlcLaundryBackend.config.security.SecurityConfiguration;
import com.ariefyantobayu.SdlcLaundryBackend.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDateTime;

@Repository
public class UserRepositoryImpl {

    @Autowired
    EntityManager entityManager;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Transactional
    public Long createUser(User user, Long roleId) {
        BigInteger lastInsertId = BigInteger.valueOf(-1);

        Query queryLastInsertId = entityManager.createNativeQuery("SELECT last_insert_id();");
        Query queryAddress = entityManager.createNativeQuery(
                "INSERT INTO address (created_at) VALUES (:create)"
        );
        queryAddress.setParameter("create", Instant.now());
        queryAddress.executeUpdate();
        lastInsertId = (BigInteger) queryLastInsertId.getSingleResult();

        Query queryUser = entityManager.createNativeQuery(
                "INSERT INTO user(email, password, username, address_id, created_at) " +
                        "VALUES(:email,:password,:username, :addressId, :create)"
        );

        queryUser.setParameter("email", user.getEmail());
        queryUser.setParameter("password", bCryptPasswordEncoder.encode(user.getPassword()));
        queryUser.setParameter("username", user.getEmail());
        queryUser.setParameter("addressId", lastInsertId);
        queryUser.setParameter("create", LocalDateTime.now());
        queryUser.executeUpdate();
        lastInsertId = (BigInteger) queryLastInsertId.getSingleResult();

        Query queryRole = entityManager.createNativeQuery(
                "INSERT INTO user_roles(user_id, roles_id) VALUES(:userId, :rolesId)"
        );
        queryRole.setParameter("userId", lastInsertId);
        queryRole.setParameter("rolesId", roleId);
        queryRole.executeUpdate();

        return lastInsertId.longValue();
    }
}
