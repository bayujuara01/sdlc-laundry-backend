package com.ariefyantobayu.SdlcLaundryBackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ariefyantobayu.SdlcLaundryBackend.entity.Status;

public interface StatusRepository extends JpaRepository<Status, Long> {

}
