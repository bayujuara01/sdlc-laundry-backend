package com.ariefyantobayu.SdlcLaundryBackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomService;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Service;

public interface ServiceRepository extends JpaRepository<Service, Long> {
	@Query(value = "SELECT id, name, price FROM service", nativeQuery = true)
	List<CustomService.GetNamePrice> GetPartnerService();
}
