package com.ariefyantobayu.SdlcLaundryBackend.dto;

public interface CustomUser {
	public interface GetNearby {
		public String getUsername();
		public Double getLongitude();
		public Double getLatitude();
		public Double getDistance();
	}
    public interface Auth {
        public Long getId();
        public String getEmail();
        public String getPassword();
    }
}
