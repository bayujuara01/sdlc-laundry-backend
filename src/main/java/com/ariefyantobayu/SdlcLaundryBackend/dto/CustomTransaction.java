package com.ariefyantobayu.SdlcLaundryBackend.dto;

public interface CustomTransaction {
	public interface GetTransByUserId {
		public String getCustomerName();
		public Integer getGrandTotal();
		public Boolean getPaymentStatus();
		public String getPartnerName();
	}
}
