package com.ariefyantobayu.SdlcLaundryBackend.dto;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.SdlcLaundryBackend.entity.Status;
import com.ariefyantobayu.SdlcLaundryBackend.entity.User;

public interface CustomFields {
	public interface getDataAllTransactions{
		public Long getId();
		public Integer getGrandTotal();
		public Boolean getPaymentStatus();
		public Double getLatitude();
		public Double getLongitude();
		public Timestamp getCreated_at();
		public User getUser();
		public User getPartner();
		public Status getStatus();
	}
//	
//	@Query(value="SELECT t.created_at, t.grand_total, t.latitude, t.longitude, \r\n"
//			+ "	   t.payment_status, t.partner_id, s.status_name, t.user_id \r\n"
//			+ "       FROM transaction t \r\n"
//			+ "       JOIN status s \r\n"
//			+ "       WHERE t.status_id = s.id", nativeQuery=true)
	
	public interface getDataTransactionByIdPartner{
		public Long getId();
		public Timestamp getCreated_at();
		public Integer getGrandTotal();
		public Double getLatitude();
		public Double getLongitude();
		public Boolean getPaymentStatus();
		public Long getPartnerId();
		public String getStatusName();
		public Long getUserId(); 
		
		
		
	}

}
