package com.ariefyantobayu.SdlcLaundryBackend.dto;

public class OrderItemDto {
	private Long itemId;
	private Integer qty;
	
	public OrderItemDto() {}
	
	public OrderItemDto(Long itemId, Integer qty) {
		super();
		this.itemId = itemId;
		this.qty = qty;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	
}
