package com.ariefyantobayu.SdlcLaundryBackend.dto;

public interface CustomService {
	public interface GetNamePrice {
		public String getId();
		public String getName();
		public String getPrice();
	}
}
