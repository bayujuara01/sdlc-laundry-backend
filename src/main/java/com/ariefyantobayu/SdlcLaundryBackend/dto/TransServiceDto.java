package com.ariefyantobayu.SdlcLaundryBackend.dto;

public class TransServiceDto {
	private Long customerId;
	private Long serviceId;
	private Integer qty;

	public TransServiceDto() {
	}

	public TransServiceDto(Long customerId, Long serviceId, Integer qty) {
		super();
		this.customerId = customerId;
		this.serviceId = serviceId;
		this.qty = qty;
	}

	public Long getServiceId() {
		return serviceId;
	}

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
