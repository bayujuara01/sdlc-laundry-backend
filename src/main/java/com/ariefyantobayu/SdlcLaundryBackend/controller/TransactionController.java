package com.ariefyantobayu.SdlcLaundryBackend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomTransaction;
import com.ariefyantobayu.SdlcLaundryBackend.dto.TransServiceDto;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Address;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Service;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Status;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Transaction;
import com.ariefyantobayu.SdlcLaundryBackend.entity.TransactionDetail;
import com.ariefyantobayu.SdlcLaundryBackend.entity.User;
import com.ariefyantobayu.SdlcLaundryBackend.exception.RecordNotFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.repository.AddressRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.ServiceRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.TransactionDetailRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.TransactionRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepository;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;

@RestController
public class TransactionController {
	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	TransactionDetailRepository transactionDetailRepository;

	@Autowired
	ServiceRepository serviceRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AddressRepository addressRepository;

	@GetMapping(value = "/customer/{id}/transaction")
	public ResponseEntity<Response> GetTransByUserId(@PathVariable Long id) {
		List<CustomTransaction.GetTransByUserId> result = transactionRepository.GetTransByUserId(id);
		return Responses.ofData(result);
	}

	@PostMapping(value = "/partner/{id_partner}/transaction", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> CreateNewTransaction(@PathVariable(name = "id_partner") long idPartner,
			@RequestBody List<TransServiceDto> payload) {

		Transaction transaction = new Transaction();
		List<TransactionDetail> transactionDetails = new ArrayList<>();
		int grandTotal = 0;

		for (TransServiceDto serviceList : payload) {
			Optional<Service> serviceRequest = serviceRepository.findById(serviceList.getServiceId());
			TransactionDetail transDetail = new TransactionDetail();
			int total = 0;
			if (serviceRequest.isEmpty()) {
				System.out.println("Service not found");
				throw new RecordNotFoundException("Service not found");
			}

			total = serviceRequest.get().getPrice() * serviceList.getQty();
			grandTotal += total;
			transDetail.setService(serviceRequest.get());
			transDetail.setQty(serviceList.getQty());
			transDetail.setPrice(serviceRequest.get().getPrice());
			transDetail.setTotal(total);
			transDetail.setTransaction(transaction);
			transactionDetails.add(transDetail);
		}

		Optional<User> partnerRequest = userRepository.findById(idPartner);
		Optional<User> customerRequest = userRepository.findById(payload.get(0).getCustomerId());
		Status status = new Status();

		if (partnerRequest.isEmpty() || customerRequest.isEmpty()) {
			throw new RecordNotFoundException("Service not found");
		}
		status.setId(idPartner);
		Address partner = addressRepository.getById(idPartner);
		
		transaction.setGrandTotal(grandTotal);
		transaction.setPartner(partnerRequest.get());
		transaction.setUser(customerRequest.get());
		transaction.setTransactionDetails(transactionDetails);
		transaction.setPaymentStatus(false);
		transaction.setStatus(status);
		
		transaction.setLatitude(partner.getLatitude());
		transaction.setLongitude(partner.getLongitude());

		Transaction savedTrans = transactionRepository.save(transaction);
		for (TransactionDetail transactionDetail : transactionDetails) {
			transactionDetailRepository.save(transactionDetail);
		}

		return Responses.ofData(savedTrans, "TEST OK");
	}
}
