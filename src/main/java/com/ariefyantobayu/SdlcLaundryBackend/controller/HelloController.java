package com.ariefyantobayu.SdlcLaundryBackend.controller;

import java.util.List;

import com.ariefyantobayu.SdlcLaundryBackend.entity.User;
import com.ariefyantobayu.SdlcLaundryBackend.exception.RecordNotFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepository;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class HelloController {

    @Autowired
	private AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/test/{id}/lol")
    public String test(@PathVariable(name = "id") Integer id) {
        return id.toString();
    }

    @GetMapping("/hello")
    public String hello() {
        var user = userRepository.findByEmail("ariefyantobayu@gmail.com");
        return user.isPresent() ? "Present" : "Error";
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody User user) throws Exception {

		try {
            System.out.println("OK");
			authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword())
            );
		}
		catch (BadCredentialsException e) {

			throw new Exception("Incorrect username or password", e);
		}
        

		return ResponseEntity.ok("Test");
	}

    @GetMapping("/admin")
    public String admin() {
        return "Admin Area";
    }

//    @GetMapping("/partner")
//    public String partner() {
//        return "Partner Area";
//    }
}
