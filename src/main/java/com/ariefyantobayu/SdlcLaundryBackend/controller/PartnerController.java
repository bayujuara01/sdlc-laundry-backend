package com.ariefyantobayu.SdlcLaundryBackend.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.SdlcLaundryBackend.entity.Transaction;
import com.ariefyantobayu.SdlcLaundryBackend.entity.TransactionDetail;
import com.ariefyantobayu.SdlcLaundryBackend.entity.User;
import com.ariefyantobayu.SdlcLaundryBackend.exception.RecordNotFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomFields;
import com.ariefyantobayu.SdlcLaundryBackend.dto.OrderItemDto;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Item;
import com.ariefyantobayu.SdlcLaundryBackend.entity.OrderDetail;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Orders;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Status;
import com.ariefyantobayu.SdlcLaundryBackend.repository.ItemRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.OrderDetailRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.OrderRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.StatusRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.TransactionDetailRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.TransactionRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepository;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;

@RestController
public class PartnerController {
	
	@Autowired
	TransactionRepository repoTransaction;
	
	@Autowired
	TransactionDetailRepository repoTransactionDetail;
	
	@Autowired
	ItemRepository repoItem;
	
	@Autowired
	UserRepository repoUser;
	
	@Autowired
	OrderDetailRepository repoOrderDetail;
	
	@Autowired
	OrderRepository repoOrder;
	
	@Autowired
	StatusRepository repoStatus;
	
	//API Get All Transaction List [GET] /api/partner/transactions
	@GetMapping(value="/partner/{id_partner}/transactions", produces="application/json")
	public ResponseEntity<Response> getAllTransactions(@PathVariable Long id_partner) {
		List<Transaction> all = repoTransaction.getAllTransactions(id_partner);
		return Responses.ofData(all, all.size() > 0 ? "OK" : "Empty");
	}
	
	@GetMapping(value="/partner/{id_partner}/transactions/details", produces="application/json")
	public ResponseEntity<Response> getAllTransactionsDetails(@PathVariable Long id_partner, @RequestParam("transaction") Long id_transaction) {
		List<TransactionDetail> all =  repoTransactionDetail.getTransactionDetailById(id_transaction);
		return Responses.ofData(all, all.size() > 0 ? "OK" : "Empty");
	}

	@GetMapping(value="/partner/{id_partner}/transactions/new", produces="application/json")
	public ResponseEntity<Response> getNewTransactions(@PathVariable Long id_partner) {
		List<Transaction> all = repoTransaction.getNewTransactions(id_partner);
		return Responses.ofData(all, all.size() > 0 ? "OK" : "Empty");
	}
	
	@GetMapping(value="/partner/{id_partner}/transactions/onprocess", produces="application/json")
	public ResponseEntity<Response> getOnProcessTransactions(@PathVariable Long id_partner) {
		List<Transaction> all = repoTransaction.getOnProcessTransactions(id_partner);
		return Responses.ofData(all, all.size() > 0 ? "OK" : "Empty");
	}
	
	@GetMapping(value="/partner/{id_partner}/transactions/completed", produces="application/json")
	public ResponseEntity<Response> getCompletedTransactions(@PathVariable Long id_partner) {
		List<Transaction> all = repoTransaction.getCompletedTransactions(id_partner);
		return Responses.ofData(all, all.size() > 0 ? "OK" : "Empty");
	}
	
	
	//API Get Grand Total/Income By Day [GET] /api/partner/income?date=2021-11-15
	@GetMapping(value="/partner/{id_partner}/income", produces="application/json")
	public ResponseEntity<Response> getSumGrandTotalByDate(@PathVariable Long id_partner, @RequestParam("date") String date) {
		Integer all = repoTransaction.getSumGrandTotalByDate(date, id_partner);
		return Responses.ofData(all!=null? all :0, all !=null ? "OK" : "No Income");
	}
	
	// Masih salah
	@GetMapping(value = "/partner/{id_partner}/order/{id_order}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getOrderById(
			@PathVariable(name = "id_partner") Long idPartner, 
			@PathVariable(name = "id_order") Long idOrder) {
	
		Optional<Orders> lisOrderStatus = repoOrder.findById(idOrder);
		return Responses.ofData(lisOrderStatus.get());
	}
	
	
	//Purchasing Laundry Supplies
	@PostMapping(value="/partner/{id_partner}/order", 
			consumes= MediaType.APPLICATION_JSON_VALUE, 
			produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> createNewOrder(@PathVariable(name = "id_partner") Long idPartner, @RequestBody List<OrderItemDto> payload){
		Orders order = new Orders();
		List<OrderDetail> orderDetails = new ArrayList<>();
		int grandTotal = 0;
		
		for (OrderItemDto item : payload) {
			Optional<Item> itemRequest = repoItem.findById(item.getItemId());
			OrderDetail orderDetail = new OrderDetail();
			int total = 0;
			if (itemRequest.isEmpty()) {
				System.out.println("item not found");
				throw new RecordNotFoundException("Item not found");
			}

			total = itemRequest.get().getPrice() * item.getQty();
			grandTotal += total;
			orderDetail.setItem(itemRequest.get());
			orderDetail.setQty(item.getQty());
			orderDetail.setTotal(total);
			orderDetail.setOrder(order);
			orderDetails.add(orderDetail);
		}
		
		Optional<User> adminRequest = repoUser.findById(1L);
		Optional<User> partnerRequest = repoUser.findById(idPartner);
		Status status = new Status();
		
		
		if (adminRequest.isEmpty() || partnerRequest.isEmpty()) {
			throw new RecordNotFoundException("Item not found");
		}
		
		status.setId(1L);
		
		order.setGrandTotal(grandTotal);
		order.setAdmin(adminRequest.get());
		order.setPartner(partnerRequest.get());
		order.setOrderDetails(orderDetails);
		order.setPaymentStatus(false);
		order.setStatus(status);
		
		Orders savedOrder =  repoOrder.save(order);

		return Responses.ofData(savedOrder,"TEST OK");
	}
	
	
	@PostMapping(value="/partner/{id_partner}/transaction/{id_transaction}",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> updateStatus(@PathVariable(name="id_transaction") Long idTransaction, 
			@PathVariable(name="id_partner") Long idPartner, 
			@RequestBody Status status){
		Optional<Transaction> transactionRequest = repoTransaction.getTransactionByIdAndPartner(idPartner, idTransaction);
		
		if(transactionRequest.isEmpty()) {
			throw new RecordNotFoundException("item not found");
		}
		
		Transaction transaction = transactionRequest.get();
		Status getStatus = repoStatus.getById(status.getId());
		transaction.setStatus(getStatus);
		Transaction updateTransaction = repoTransaction.save(transaction);
		Optional<Transaction> updatedTransaction = repoTransaction.findById(idTransaction);
//		updateTransaction = updatedTransaction.get();
		
		return Responses.ofData(updateTransaction);
//		return Responses.ofData("OK", "OK");
	}
	
}
