package com.ariefyantobayu.SdlcLaundryBackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomService;
import com.ariefyantobayu.SdlcLaundryBackend.repository.ServiceRepository;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;

@RestController
public class ServiceController {

	@Autowired
	ServiceRepository serviceRepository;

	@GetMapping(value = "/services", produces = "application/json")
	public ResponseEntity<Response> GetPartnerServices() {
		List<CustomService.GetNamePrice> services = serviceRepository.GetPartnerService();

		return Responses.ofData(services);
	}
}
