package com.ariefyantobayu.SdlcLaundryBackend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.SdlcLaundryBackend.dto.CustomUser;
import com.ariefyantobayu.SdlcLaundryBackend.repository.AddressRepository;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;

@RestController
public class UserController {
	@Autowired
	AddressRepository addressRepository;

	@GetMapping(value = "/partner")
	public ResponseEntity<Response> GetNearbyPartner(@RequestParam(name = "lat") Double lat,
			@RequestParam(name = "lng") Double lng,
			@RequestParam(name = "maxDistance", required = false) Optional<Integer> maxD,
			@RequestParam(name = "maxShown", required = false) Optional<Integer> maxS) {
		double userLongitude = lng;
		double userLatitude = lat;
		int maxDistance = maxD.isPresent() ? maxD.get() : 5;
		int maxShown = maxS.isPresent() ? maxS.get() : 10;
		List<CustomUser.GetNearby> result = addressRepository.GetNearbyPartner(userLatitude, userLongitude, maxDistance,
				maxShown);

		return Responses.ofData(result);
	}
}