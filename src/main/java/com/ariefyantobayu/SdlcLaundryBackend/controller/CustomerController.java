package com.ariefyantobayu.SdlcLaundryBackend.controller;

import com.ariefyantobayu.SdlcLaundryBackend.entity.User;
import com.ariefyantobayu.SdlcLaundryBackend.exception.RecordNotFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.exception.RegistrationEmailFoundException;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepositoryImpl;
import com.ariefyantobayu.SdlcLaundryBackend.response.Response;
import com.ariefyantobayu.SdlcLaundryBackend.response.Responses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRepositoryImpl userRepositoryImpl;

    @PostMapping(value = "/login",
        consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> customerLogin(@Valid @RequestBody User user) {
       return null;
    }

    @PostMapping(value = "/register",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> customerRegister(@Valid @RequestBody User user) {
        Optional<User> userRequest = userRepository.findByEmailNative(user.getEmail());

        if (userRequest.isPresent()) {
            throw new RegistrationEmailFoundException();
        }
        Long userId = userRepositoryImpl.createUser(user, 1L);
        return Responses.ofData(userId);
    }
}
