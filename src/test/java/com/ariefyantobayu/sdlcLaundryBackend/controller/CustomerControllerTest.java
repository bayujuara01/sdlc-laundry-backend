package com.ariefyantobayu.sdlcLaundryBackend.controller;

import com.ariefyantobayu.SdlcLaundryBackend.controller.CustomerController;
import com.ariefyantobayu.SdlcLaundryBackend.controller.HelloController;
import com.ariefyantobayu.SdlcLaundryBackend.entity.Role;
import com.ariefyantobayu.SdlcLaundryBackend.entity.User;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepository;
import com.ariefyantobayu.SdlcLaundryBackend.repository.UserRepositoryImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class CustomerControllerTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserRepositoryImpl userRepositoryImpl;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoad() {
        assertNotNull(userRepository);
        assertNotNull(userRepositoryImpl);
    }

    @Test
    @DisplayName("Post Request Login And Valid Test")
    void whenPostRequestLoginAndValidArgument_thenCorrectResponse() throws Exception {
        Role role = new Role(1L, "USER");
        User user = new User(1L,
                "ariefyantobayu@gmail.com",
                "$2a$10$MENIebdTNcxNnksVcmyFMOolhHVg6Rfsd6bFVfsnJVsgv3I3y157m", Set.of(role));
        when(userRepository.findByEmailNative(anyString())).thenReturn(Optional.of(user));
        String requestBody = "{\"email\": \"ariefyantobayu@gmail.com\", \"password\" : \"Bayujuara01\"}";
        this.mockMvc.perform(post("/authenticate")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Post Request Login And Not Valid Test")
    void whenPostRequestLoginAndValidArgument_thenErrorResponse() throws Exception {
        Role role = new Role(1L, "USER");
        User user = new User(1L,
                "ariefyantobayu@gmail.com",
                "$2a$10$MENIebdTNcxNnksVcmyFMOolhHVg6Rfsd6bFVfsnJVsgv3I3y157m", Set.of(role));
        when(userRepository.findByEmailNative(anyString())).thenReturn(Optional.of(user));
        String requestBody = "{\"email\": \"ariefyantobayu@gmail.com\", \"password\" : \"adminNotValid\"}";
        this.mockMvc.perform(post("/authenticate")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("[Post] Customer Register And Valid Test")
    void whenPostRegisterCustomerAndValidArgument_thenCorrectResponse() throws Exception {
        Role role = new Role(1L, "USER");
        User user = new User(1L,
                "ariefyantobayu@gmail.com",
                "Bayujuara01", Set.of(role));
        when(userRepository.findByEmailNative(anyString())).thenReturn(Optional.empty());
        when(userRepositoryImpl.createUser(any(User.class), anyLong())).thenReturn(1L);
        String requestBody = "{\"email\": \"ariefyantobayu@gmail.com\", \"password\" : \"Bayujuara01\"}";
        this.mockMvc.perform(post("/customer/register")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("[Post] Customer Register And Already Email Test")
    void whenPostRegisterCustomerAndEmailAlready_thenErrorResponse() throws Exception {
        Role role = new Role(1L, "USER");
        User user = new User(1L,
                "ariefyantobayu@gmail.com",
                "Bayujuara01", Set.of(role));
        when(userRepository.findByEmailNative(anyString())).thenReturn(Optional.of(user));
        String requestBody = "{\"email\": \"ariefyantobayu@gmail.com\", \"password\" : \"Bayujuara01\"}";
        this.mockMvc.perform(post("/customer/register")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("400")));
    }

    @Test
    @DisplayName("[Post] Customer Register And Not Valid Argument Formation Test")
    void whenPostRegisterCustomerAndNotValidArgumentFormation_thenCorrectResponse() throws Exception {
        Role role = new Role(1L, "USER");
        User user = new User(1L,
                "ariefyantobayu@gmail.com",
                "Bayujuara01", Set.of(role));
        when(userRepository.findByEmailNative(anyString())).thenReturn(Optional.empty());
        String requestBody = "{\"email\": \"ariefyantobayu@gmail.com\", \"password\" : \"admin\"}";
        this.mockMvc.perform(post("/customer/register")
                        .content(requestBody)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("400")));
    }
}